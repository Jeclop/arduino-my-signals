/*

    Copyright (C) 2017 Libelium Comunicaciones Distribuidas S.L.
   http://www.libelium.com

    By using it you accept the MySignals Terms and Conditions.
    You can find them at: http://libelium.com/legal

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Version:           2.0
    Design:            David Gascon
    Implementation:    Luis Martin / Victor Boria
*/


#include <MySignals.h>


uint8_t pulsioximeter_state = 0;
bool pulso=false;


void setup() 
{
  Serial.begin(115200);
  MySignals.begin();
}

void loop() 
{
leerbuffer();
if(pulso)
 {
    if (MySignals.spo2_micro_detected == 0 )
      {
       uint8_t statusPulsioximeter = MySignals.getStatusPulsioximeterGeneral();
       if (statusPulsioximeter == 1)
          {
          MySignals.spo2_micro_detected = 1;
          }
       else if (statusPulsioximeter == 2)
          {
          MySignals.spo2_micro_detected = 0;
          }
       else
          {
          MySignals.spo2_micro_detected = 0;
          }
      }
    if (MySignals.spo2_micro_detected == 1)
     {
      uint8_t getPulsioximeterMicro_state = MySignals.getPulsioximeterMicro();
      if (getPulsioximeterMicro_state == 1)
        {
         Serial.print(F("#"));
         Serial.println(MySignals.pulsioximeterData.BPM);
         Serial.println(MySignals.pulsioximeterData.O2);
        }
      else if (getPulsioximeterMicro_state == 2)
        {}
      else
        {
      MySignals.spo2_micro_detected = 0;
        }
    
delay(1000);
     }
 }
}

void leerbuffer(){
  char data;
  if(Serial.available()>0)
    {
     data=Serial.read();
     if(data=='#')
      {
       pulso=true;
       MySignals.initSensorUART();
       MySignals.enableSensorUART(PULSIOXIMETER);
      }
     if(data=='$')
      {
       pulso=false;
      }  
    }
}









